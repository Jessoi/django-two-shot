from django.urls import path
from receipts.views import receipt_list, create_receipt, receipt_account, receipt_category, create_receipt_category, create_account
urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", receipt_category, name="categories"),
    path("categories/create/", create_receipt_category, name="create_categories"),
    path("accounts/", receipt_account, name="accounts"),
    path("accounts/create/", create_account, name="create_account"),
]