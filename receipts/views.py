from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.contrib.auth.decorators import login_required

# Create your views here.
def redirect_to_receipt_list(request):
    return redirect("home")

@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipts
    }
    return render(request, "receipts/list.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            form.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form
    }
    return render(request, "receipts/create.html", context)

@login_required
def receipt_category(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": category
    }
    return render(request, "receipts/category.html", context)

@login_required
def receipt_account(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        "account_list": account
    }
    return render(request, "receipts/account.html", context)

@login_required
def create_receipt_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("categories")
    else:
        form = ExpenseCategoryForm()

    context = {
        "form": form
    }
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("accounts")
    else:
        form = AccountForm()

    context = {
        "form": form
    }
    return render(request, "receipts/create_account.html", context)